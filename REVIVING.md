# Reviving

Things that changed since the last release of this app (In case you want to put them in the release notes):

- Added Russian, Ukrainian, German, French, Turkish, Occitan translations
- Removed ~85% of fonts (all the ugly ones)
- Changed versioning scheme (From major.minorbBugfix to major.minor)
- increased default window size
- prettified font names
- completely revamped UI
- added searching for fonts

Things that need to be fixed somehow:

- The font preview cards should fade out gently towards the right.

In case you have any questions, feel free to ask me on [Matrix](https://matrix.to/#/@gregorni:gnome.org) or [Mastodon](https://fosstodon.org/@gregorni)