# fonts_list.py
#
# Copyright 2023 Calligraphy Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

FONTS_LIST = {
    "3D": "3-d",
    "5-line Oblique": "5lineoblique",
    "Acrobatic": "acrobatic",
    "Alligator Italic": "alligator",
    "Alligator": "alligator2",
    "Avatar": "avatar",
    "Banner 3D": "banner3-D",
    "Bell": "bell",
    "Big": "big",
    "Block": "block",
    "Chunky": "chunky",
    "Coin Stack": "coinstak",
    "Cosmic": "cosmic",
    "Crawford": "crawford",
    "Cyber Large": "cyberlarge",
    "Def Leppard": "defleppard",
    "Dot Matrix": "dotmatrix",
    "Epic": "epic",
    "Fraktur": "fraktur",
    "Fuzzy": "fuzzy",
    "Goofy": "goofy",
    "Gothic": "gothic",
    "Graffiti": "graffiti",
    "Hollywood": "hollywood",
    "Invita": "invita",
    "Isometric": "isometric3",
    "Jazmine": "jazmine",
    "Kban": "kban",
    "Letters": "letters",
    "Marquee": "marquee",
    "Mini": "mini",
    "Nancyj": "nancyj",
    "NV Script": "nvscript",
    "Poison": "poison",
    "Puffy": "puffy",
    "Relief": "relief",
    "Roman": "roman",
    "Script": "script",
    "Serifcap": "serifcap",
    "Shimrod": "shimrod",
    "Small": "small",
    "Speed": "speed",
    "Star Wars": "starwars",
    "Thin": "thin",
    "Tombstone": "tombstone",
    "Universe": "univers",
    "USA Flag": "usaflag",
}
