using Gtk 4.0;
using Adw 1;

template $CalligraphyWindow: Adw.ApplicationWindow {
  title: "Calligraphy";
  height-request: 296;
  width-request: 300;

  content: Adw.ToastOverlay toast_overlay {
    child: Adw.ToolbarView {
      bottom-bar-style: raised;

      content: Adw.NavigationView main_nav_view {
        Adw.NavigationPage {
          title: _("Calligraphy");

          Adw.ToolbarView {
            [top]
            Adw.HeaderBar {
              [start]
              ToggleButton search_toggle {
                icon-name: "folder-saved-search-symbolic";
                tooltip-text: _("Search Fonts");
                sensitive: false;
                active: bind search_bar.search-mode-enabled bidirectional;
              }

              [end]
              MenuButton {
                primary: true;
                menu-model: primary_menu;
                icon-name: "open-menu-symbolic";
                tooltip-text: _("Main Menu");
              }
            }

            [top]
            SearchBar search_bar {
              sensitive: false;

              child: Adw.Clamp {
                SearchEntry search_entry {
                  search-delay: 100;
                  placeholder-text: _("Search fonts");
                }
              };
            }

            content: Stack welcome_stack {
              transition-type: crossfade;

              StackPage {
                name: "welcome";

                child: Adw.StatusPage {
                  icon-name: "io.gitlab.gregorni.Calligraphy";
                  title: _("Create ASCII Banners");
                  description: _("Type something to get started");
                };
              }

              StackPage {
                name: "fonts-list";

                child: ScrolledWindow {
                  Adw.Clamp {
                    maximum-size: 1000;
                    tightening-threshold: 600;

                    FlowBox preview_list_flowbox {
                      selection-mode: none;
                      margin-bottom: 24;
                      margin-start: 12;
                      margin-end: 12;
                      column-spacing: 6; // The extra spacing is handled in style.css
                      max-children-per-line: 2;
                      homogeneous: true;
                      valign: start;
                    }
                  }
                };
              }

              StackPage {
                name: "no-results";

                child: Adw.StatusPage {
                  title: _("No Results Found");
                  description: _("Try a different search");
                  icon-name: "edit-find-symbolic";
                  vexpand: true;
                };
              }
            };
          }
        }
      };

      [bottom]
      Adw.Clamp {
        maximum-size: 500;
        tightening-threshold: 400;

        styles [
          "toolbar",
        ]

        Box {
          spacing: 5;

          Overlay {
            css-name: "entry";

            styles [
              "no-right-padding",
            ]

            Box {
              ScrolledWindow {
                vscrollbar-policy: external;
                max-content-height: 200;
                propagate-natural-height: true;

                TextView input_text_view {
                  margin-top: 6;
                  margin-bottom: 6;
                  hexpand: true;
                  input-purpose: alpha;
                }
              }

              Button clear_input_btn {
                icon-name: "edit-clear-symbolic";
                halign: end;
                visible: false;
                tooltip-text: _("Clear Entry");
                vexpand: false;
                valign: center;

                styles [
                  "clear-input-button",
                ]
              }
            }

            [overlay]
            Label hint_label {
              label: _("Type here…");
              halign: start;
              can-target: false;

              styles [
                "dim-label"
              ]
            }
          }

          Revealer warning_revealer {
            transition-type: slide_left;

            MenuButton {
              icon-name: "dialog-warning-symbolic";
              width-request: 30;
              tooltip-text: _("Warning");
              direction: up;

              styles [
                "warning",
              ]

              popover: Popover {
                child: Label {
                  label: _("Non-alphabetic characters may not be supported in every font");
                };
              };
            }
          }
        }
      }
    };
  };
}

menu primary_menu {
  section {
    item {
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      label: _("_About Calligraphy");
      action: "app.about";
    }
  }
}
